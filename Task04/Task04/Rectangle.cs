﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task04
{
    class Rectangle
    {
        private int width;
        private int height;

        public Rectangle(int width, int height)
        {
            this.width = width;
            this.height = height;
        }

        public int Width { get => width; set => width = value; }
        public int Height { get => height; set => height = value; }

        public void Display()
        {
            for (int i = 1; i <= Height; i++)
            {
                for (int j = 1; j <= Width; j++)
                {
                    if (((i == 1 || i == Height) || (j == 1 || j == Width))
                        ||
                        (i == 3 && (j > 2 && j < Width - 1))
                        ||
                        (j == 3) && (i > 2 && i < Height - 1)
                         ||
                        (i == Height - 2) && (j > 2 && j < Width - 1)
                         ||
                        (j == Width - 2) && (i > 2 && i < Height - 1))
                    {
                        Console.Write("#"); //prints the border
                    }
                    else
                    {
                        Console.Write(" "); //prints inside other than border
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
