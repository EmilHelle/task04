﻿using System;

namespace Task04
{
    class Program
    {
        static void Main(string[] args)
        {
            //get user input width and height
            Console.WriteLine("Please write width and height");
            Console.Write("Enter Width :");
            int width = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter Height :");
            int height = Convert.ToInt32(Console.ReadLine());


            //create rectangle and display
            Rectangle rectangle = new Rectangle(width, height);
            rectangle.Display();

        }
    }
}
